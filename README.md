# RxJava Playground

The formal documentation for this repo lives in the `docs` folder.

This can served as html using Jekyll:

1. [Install Jekyll](https://jekyllrb.com/docs/installation/)
1. `cd docs && bundle exec jekyll serve`

This repository is also set up to serve this documentation from [Gitlab Pages](https://about.gitlab.com/product/pages/).

If this site is stored in a gitlab repository that supports pages then try navigating
to `http://projectowner.gitlab.io/rx-playground`. Substituting `projectowner` with
the owner of the project (your username if you own it, or the group name if it's owned by a group).

If you are using a self hosted gitlab repository then substitute `gitlab.io` appropriately.

Just be aware that gitlab pages can take some time to update.