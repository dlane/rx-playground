---
title: project roadmap
---

# Project Roadmap

This file contains a rough road-map for this project. This is a living document and
liable to change.

Note that the most useful resource for formal definitions is [the main reactivex site](http://reactivex.io). 
The JavaDoc for RxJava comes after that.

## Completables

Completables have four events: subscribe, complete, error, unsubscribe. Unsubscribe is an advanced topic
and we don't plan to tackle this yet.

### Subscribing to a completable

Write code that returns a Completable that completes in one second and an error
variant.

Explain subscribing to both and the behaviour.

Note that you can subscribe multiple times to a single observable, but that's handled later.

### The Complete event

Demonstrate `doOnComplete()`.

### Using a Cold Observable

Demonstrate that you can store a cold completable (like the support one) and subscribe to it
multiple times.

Demonstrate `doOnComplete()` creates a new Completable that behaves identically to the first.

### The Error event

Demonstrate `doOnError`.

### Explaining When to Schedule

Note that the only guarantee that something is run on a particular scheduler
is to use an `observeOn` before every call. Explain that the general philosophy
is that unless it's a blocking operation like IO or a big computation you
don't care what thread it's on because it's comparably instantaneous.

### Scheduling using observeOn

Demonstrate thread change using obeserveOn. Demonstrate the use of multiple
observeOn calls in a single chain. Demonstrate it for error events.
Demonstrate that it affects the callbacks in the `subscribe()` call.

Note to check the later section on how scheduling works when chaining two
completables.

### the Subscribe event

Demonstrate `fromAction()`. Show that it's called every time you subscribe. Show that
the completable waits to complete.

## Other ways to create an Observable.

Demonstrate `complete()` and `error()`.

### Scheduling using subscribeOn

Use RxJava2 for this section. Note this and explain how subscribe-time scheduling
in Rx1 is broken. Demonstrate this after all the propper demonstrations.

Demonstrate that subscribeOn() effects where the fromAction() callback is called
and the thread that things are observed on on the onComplete call. Demonstrate that
putting an observeOn in the middle changes this.

Demonstrate how to have a chain with multiple subscribe-time side effects in different
threads using onSubscribe and subscribeOn.

### Adding Side Effects

`doOnComplete()`

### Chaining Completables (needs to be done after scheduling!)

#### concat()

Demonstrate `Completable.concat()` with two completables that subscribe on different schedulers.
Demonstrate what happens if you have a Completable with no scheduler specified sandwiched between
two which run on different schedulers.

Demonstrate `andThen()`.

### The "Luke Warm Observable" Code Smell

Demonstrates what happens when you write a function which generates a Completable
by doing half the work at construction time and half the work at subscribe time.
Explain how this could break things and cause resource leaks.

Demonstrate how to patch around this using `defer` if it's impossible to refactor the original code.

#### merge()

Demonstrate `Completable.merge()` with completables that subscribe on different schedulers. 
Show that all Completables are subscribed to at once.

Demonstrate it again with completables with no subscribeOn. Show that you end up with nothing
running in parallel.

Then put the fear of codethulu into them by merging observables that are randomly determined to either
have no scheduler or a newThreadScheduler.  Explain that no parallelism is guaruanteed, but
 serialism isn't guaranteed either. only that the system may run these at the same time if it is possible.

Demonstrate what happens when multiple errors are thrown.
Demonstrate safe handling for multiple errors (IE do the handling before merging).
Demonstrate `mergeWith()`.
Demonstrate `mergeDelayError()` and what happens when there are multiple errors then.

### Unsubscribe Events

I haven't looked at unsubscribe seriously so this might take some more fleshing out.

Demonstrate how to unsubscribe. Demonstrate what happens at different points in the chain
and wrt scheduling. 

`unsubscribeOn()` exists though.

### Blocking Completables

Show they exist and how to use them.
Show that the exception you get out is wrapped in a RuntimeException so can't be used in a try/catch
for specific types.

## Using Single

Brief explanation that a Single is exactly like a Completable, except that the Complete
event has a single value attached and is called `success`.

### Creating a single

Demonstrate `fromCallable`, `just` and `from` and note their behaviour on subscribe time.
Show how multiple subscriptions to any of them will cause the subscribe-time side effect to be
repeated to regenerate the events.

### Simple Common Operators

Demonstrate `doOnSuccess` and `doOnErrror`.
Demonstrate `map`.

### Joining Singles

Note that you can do this but it results in Observables. These are covered in their own section.

### Common Complex Operators: flatMap and Inner Observables

Demonstrate `flatMap`.
Demonstrate what happens when an inner observable uses a different scheduler.
Note that guaranteeing what thread it comes in on is probabilistic, ala 
[this](https://stackoverflow.com/questions/51677721/does-the-scheduler-on-a-flatmap-observable-affect-the-scheduler-on-the-outer-o]
stack overflow question. Note that this is one of the reasons to assume nothing wrt threads
in Rx java - the only safe ordering guarantees are those provided by the operators themselves.

Basically the subscription of the inner observable will start on whatever thread
the outer observable observes on, however will then follow the standard routine
depeding on observeOn or subscribeOn operators. Note that this is general
behaviour and goes out the window when you start looking at the `Observable` class.
Again, the only safe guarantee on ordering is though the operator definitions.

### Resource Handling in Singles

Explain `using()`. Show how it works if you subscribe to the same cold observable twice.
Explain that when the docs say `unsubscription` they mean `termination`. I should really make a MR
to fix that.

### Revisiting the Luke Warm Observable Code Smell

Show a theoretical resource leak created by creating a resource at construction time
and then disposing of it using `onTerminate()`, but not subscribing to the Single.

### Mapping to Completable

Demonstrate `toCompletable()`.
Demonstrate `flatMapCompletable()`. Note it has the same scheduling behaviour as
`flatMap()`.

### Blocking Singles

Demonstrate blocking singles.

## Moving on to Observable

Note that Observables are like Singles but unlike them in the sense that they introduce complex ordering issues
and back-pressure problems. Note that flatMap is a commonly used operator but potentially dangerous.

Observables have one `subscribe` event, zero or more `next` events with a value attached to each, and one terminating event
(either `complete` or `error`). They may also have an `unsubscribe` event, but this is an advanced topic and not covered
 yet. In addition to this they also come in two forms, cold or hot, and have to deal with back-pressure.

### Creating an Observable

Note that fromAction, just and from have the same subscribe time context as Single. Demonstrate `generate()`.

### Things that will confuse you when you try to test ordering: Backpressure, Reactive Pull and the magic of flatMap

#### Cold Observables

Cold observables work in a "pull" fashion. That means it's up to the operator to request as many at a time as it
can handle. Most of the time operators will request a single item at once, however some operators request multiple
items.

concatMap, since the last time I looked, pre-fetches two items. flatMap, by its nature, will consume
items as soon as they come in.

Note that this is further confused by schedulers. If you don't specify a different scheduler inside your concatMap or
flatMap you may find that only one is requested at a time because the original thread is busy inside the mapping.

Which thread you get out of flatMap is also probabilistic. See [this](https://stackoverflow.com/questions/51677721/does-the-scheduler-on-a-flatmap-observable-affect-the-scheduler-on-the-outer-o)
stack overflow post.

In summary: checking what the ordering is by running tests is, for Observables, quite fragile and you probably shouldn't
rely on it.

#### Hot Observables

Hot observables emit on their own schedule. The only caveat is that only one call to onNext can happen at any given time.

We don't have much experience with hot observables - I'm not aware of any place we've used them.

There are strategies for dealing with hot observables emitting too much. They are along the lines of "either throw an
error or start dropping events".

### Common Simple Operators

demonstrate `map()`, `doOnNext()`.

### Contatinating Observables and Singles

Demonstrate `concat` for both Single and Observable.

Demonstrate that `concat` causes a subscription for item N only after N-1 has completed.

Demonstrate that this is not necesarily on the same thread.

Demonstrate that concatMap does the same, point out pre-fetch. Point out that this doesn't necesarily
prevent up-stream observables from firing events while concatMap is working - they just won't reach concatMap.

### Parallelism (finally!): Single.merge(), Observable.merge() and flatMap()

Demonstrate merging several singles.

Show what happens if merging singles on different schedulers.

Show what happens if a single with no scheduler change is sandwiched between two singles with scheduler changes.

Demonstrate that this is exactly the same for observables except each item may emit multiple values.

Demonstrate that down-stream observables can still be firing events at the same time as the observables in flatMap
are running.

Demonstrate one observable in a flatMap with no scheduler change, sandwiched between two on new thread schedulers.

Demonstrate a flatmap with no scheduler changes. Point out that relying on this is fragile because you then
have to enforce the behaviour of this function by encoding it into all functions that are called under it.

### Blocking Observables

They exist. Demonstrate the wrapping of exceptions in an untyped RuntimeException.

## Common Mistakes

mapping a function that returns an observable instead of a flatMap.

The equivalent using fromAction instead of defer.

## Future Work

Better description of `unsubscribe`.

Worked examples of hot observables.

Subject and friends.

`Observable.create()`

The Observable Contract.