---
title: Introduction to Rx-Playground
---

# Introduction to Rx-Playground

This repository is a step by step guide introduction to Rx. I have two main motivations
for writing this.

The first is that while you can kind of get the idea of how Rx works from reading
articles, I found that there was absolutely no substitute for having something you can
play with practically. The gap between "normal" programming and Rx is just too big
to jump in to without worked examples.

Secondly, existing introductions to Rx get you started quickly with writing code but at best only
 quickly touch on the model behind Rx and at best ignore it completely. I wanted to start
 by explaining the Reactive model rather than how to program in Rx Java. It's easy to jump
 into Rx and write your first Observable, but unless you understand the abstract model
 then things can quickly get impenetrable.
 
This repository is the introduction to RxJava that I wish I had when I started to
learn it.

## Caveats

I've used RxJava 1 because this is what is currently in use at work. However I hope that
this should provide enough grounding in the basic concepts to be able to migrate to 2.0
just by reading the upgrade notes.

We focus on the use of Cold completables, because we currently don't use any hot ones.
Expect this to change later.

## Structure

This guide is composed of muliple parts of documentation, each part of the documentation
having one or more worked examples in Java.

We start by introducing the simplest Reactive types and explaining what's going on "behind the scenes".
We then work up to more complex concepts piece by piece.

This marks a big difference between this and many other introductions. While many practical
guides start with introducing the Observable we start with the Completable and Single.
By introducing the simpler types first we can properly examine the basics before adding
extra complexity from event streams.

## Index

This guide is a work in progress and living code. [This](roadmap.html) file contains a rough road-map.