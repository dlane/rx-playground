package com.oceannaeco.dev.rx.playground.roadmap.completable;

import org.junit.Test;
import rx.Completable;
import rx.Scheduler;
import rx.schedulers.Schedulers;

import java.time.Duration;
import java.util.concurrent.Semaphore;

import static java.lang.String.format;
import static java.lang.System.out;


/**
 * This class demonstrates the basics of consuming a Completable.
 *
 * This glosses over the details of the events that happen - we handle those in another class.
 *
 * This is roughly equivalent to calling a normal function returning void, except you now need to know how to
 * wait for it to finish...
 *
 * The "easy" version in {@link #unscheduledSleepThenPrint()} is often appropriate for playing around
 * but becomes fragile and increasingly rare in any production code base. So we describe how to deal with realistic
 * cases (which happens to be really easy when you know the right magic runes).
 */
public class Subscribing {

    private static void safeSleep(Duration duration) {
        try {
            Thread.sleep(duration.toMillis());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void printWithThread(String format, String... args) {
        out.println(String.format(Thread.currentThread().getName() + ": " +  format, (Object[]) args));
    }

    private static Completable unscheduledSleepThenPrint() {
        return Completable.fromAction(() -> safeSleep(Duration.ofSeconds(1)))
                .doOnCompleted(() -> out.println("Hello!"));
    }

    private static Completable sleepThenPrint(Scheduler s) {
        return Completable.fromAction(() -> safeSleep(Duration.ofSeconds(1)))
                .doOnCompleted(() -> out.println(format("%s: Hello!", Thread.currentThread().getName())))
                .subscribeOn(s);
    }


    /**
     * So you have a Completable object and you want to do something when it's finished (or handle any error...).
     *
     * This happens if nothing occurs asynchronously, this generally isn't the case in production code though...
     */
    @Test
    public void waitForUnscheduledCompletion() {
        Completable completable = unscheduledSleepThenPrint();

        completable.subscribe(() -> out.println("Success!"), throwable -> out.println("error!" + throwable.toString()));
    }

    /**
     * This is the more common scenario... It's not printing, why? Because something is happening asynchronously...
     */
    @Test
    public void scheduledCompletionFails() {
        Completable completable = sleepThenPrint(Schedulers.newThread());

        completable.subscribe(() -> out.println("Success!"), throwable -> out.println("error!" + throwable.toString()));
    }


    /**
     * So if we tell the main thread to wait until the object completes...
     *
     */
    @Test
    public void scheduledCompletion() throws InterruptedException {
        Completable completable = sleepThenPrint(Schedulers.newThread());

        Semaphore semaphore = new Semaphore(0);

        completable.subscribe(() -> { out.println("Success!"); semaphore.release(); }, throwable -> out.println("error!" + throwable.toString()));

        printWithThread("Waiting until we complete...");
        semaphore.acquire();
        printWithThread("Done!");
    }

    /**
     * We need to do this a lot for testing things out, so luckily there are built in methods for doing this for us
     */
    @Test
    public void scheduleCompletion2() {
        Completable completable = sleepThenPrint(Schedulers.newThread());
        completable.await();
        printWithThread("Done!");
    }


}



